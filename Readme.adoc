= Hack your own program

A practice lab for SecDim's Defensive Programming training course

== Build and run

. *Fork* and clone this repository
. Run `make build` to build the program
. Run `make run` to run the program
. Go to http://localhost:8080

== Objective
Follow the instructions to complete the lab objective.
